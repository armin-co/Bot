# -*- coding: utf-8 -*-
"""
Created on Fri Dec 15 22:45:39 2017

@author: JHMLap
"""
import numpy
import socket
import pickle

from pysc2.agents import base_agent
from pysc2.lib import actions
from pysc2.lib import features


class Test(base_agent.BaseAgent):
  """An agent for testing"""
  def write(self, data):
    f = self.connection.makefile('wb', 32768)
    pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
    f.close()
    
  def read(self):
    f = self.connection.makefile('rb', 32768 )
    data = pickle.load(f)
    f.close()
    return data
    

  def setup(self, obs_spec, action_spec):
      super(Test, self).setup(obs_spec, action_spec)
      self.obs_spec = obs_spec
      serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      serversocket.bind(('localhost', 999))
      serversocket.listen(3)
      while True:
            self.connection, self.address = serversocket.accept()
            buf = self.connection.recv(64)
            if len(buf) > 0:
                self.write(self.obs_spec)
                break
            
  def step(self, obs):
    super(Test, self).step(obs)
    print("waiting for startstring")
    while True:
        buf = self.connection.recv(64)
        if len(buf) > 0:
            print("writing Obs")
            self.write(obs)
            break
    while True:
            print("waiting for command")
            command = self.read()
            if command:
                print("command '"+str(command)+"' received")
                break
    return actions.FunctionCall(command[0], command[1])
