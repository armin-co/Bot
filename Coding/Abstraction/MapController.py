import pysc2
from pysc2.lib.features import *
import numpy as np
from scipy.misc import imresize

class MapController():

    def registerObsParser(self, obsParser):
        self.obsParser = obsParser
        
    def registerUnitController(self, unitController):
        self.unitController = unitController
    
    def returnSegmentUnits(self,  XWPos, YWPos, Xrad = 0, Yrad = 0):
        """
        @param int radius : 
        @param int XPos : 
        @param GiveMapSegmentsint YPos : 
        @return set of MapSegment :
        @author
        """
        MXpercent = XWPos/self.MapX
        MYpercent = YWPos/self.MapY
        MXMaxP = min(1,max(0, XWPos + Xrad) / self.MapX)
        MXMinP = min(1,max(0, XWPos - Xrad) / self.MapX)
        MYMaxP = min(1,max(0, YWPos + Yrad) / self.MapY)
        MYMinP = min(1,max(0, YWPos - Yrad) / self.MapY)
        returnsegments = [[],[]]
        returnsegments[0] = self.XMapSegmentSets[int(MXMinP * len(self.XMapSegmentSets)) : int(MXMaxP * len(self.XMapSegmentSets))+1]
        returnsegments[1] = self.YMapSegmentSets[int(MYMinP * len(self.YMapSegmentSets)) : int(MYMaxP * len(self.YMapSegmentSets))+1]
        Xretseg = set()
        for xset in returnsegments[0]:
            Xretseg=Xretseg.union(xset)
        Yretseg = set()
        for yset in returnsegments[1]:
            Yretseg = Yretseg.union(yset)
        return Xretseg.intersection(Yretseg)
    
    def returnSegments(self,  XWPos, YWPos, Xrad = 0, Yrad = 0):
        """
        @param int radius : 
        @param int XPos : 
        @param GiveMapSegmentsint YPos : 
        @return set of MapSegment :
        @author
        """
        MXpercent = XWPos/self.MapX
        MYpercent = YWPos/self.MapY
        MXMaxP = min(1,max(0, XWPos + Xrad) / self.MapX)
        MXMinP = min(1,max(0, XWPos - Xrad) / self.MapX)
        MYMaxP = min(1,max(0, YWPos + Yrad) / self.MapY)
        MYMinP = min(1,max(0, YWPos - Yrad) / self.MapY)
        returnsegments = [[],[]]
        returnsegments[0] = self.XMapSegmentSets[int(MXMinP * (len(self.XMapSegmentSets)-1)) : int(MXMaxP * (len(self.XMapSegmentSets)-1))+1]
        returnsegments[1] = self.YMapSegmentSets[int(MYMinP * (len(self.YMapSegmentSets)-1)) : int(MYMaxP * (len(self.YMapSegmentSets)-1))+1]
        return returnsegments
        

    def _initializeMapSegmentSets(self,MapSegmentsX,MapSegmentsY):
        """
        @return MapSegment :
        @author
        """
        self.XMapSegmentSets = []
        for i in range(MapSegmentsX):
            self.XMapSegmentSets.append(set())
        self.YMapSegmentSets = []
        for i in range (MapSegmentsY):
            self.YMapSegmentSets.append(set())

    def _initWorld(self, MiniMapHeightMap):
       
       self.terrainMap = np.zeros( (self.MapX,self.MapY) )
       for xpx in range(self.MapX):
           for ypx in range(self.MapY):
               self.terrainMap[xpx,ypx] = MiniMapHeightMap[ int(MiniMapHeightMap.shape[0] * xpx/self.MapX), int(MiniMapHeightMap.shape[1] * ypx/self.MapY) ]       
       self.lookedAtMap = np.zeros((self.MapX,self.MapY))

    def updateLookedAt(self, ScreenStartX, ScreenStartY, ScreenEndX, ScreenEndY, HeightMap):
       try:
         HeightMap = imresize(HeightMap, (int(ScreenEndX-ScreenStartX), int(ScreenEndY-ScreenStartY)))
         if ScreenStartX + HeightMap.shape[0] <= self.terrainMap.shape[0] and ScreenStartY + HeightMap.shape[1] <= self.terrainMap.shape[1]:
           self.terrainMap[int(ScreenStartY): int(ScreenStartY + HeightMap.shape[1]),int(ScreenStartX): int(ScreenStartX+HeightMap.shape[0])] = HeightMap
           self.lookedAtMap = self.lookedAtMap + 1
           self.lookedAtMap[ int(ScreenStartY): int(ScreenStartY + HeightMap.shape[1]), int(ScreenStartX): int(ScreenStartX+HeightMap.shape[0])] = 0
         else:
           print("MapController: ScreenStartpoints out of bounds")
       except:
           print("MapController: Error Updating Map")
    
    def __init__(self, MapX,MapY,MiniMapX,MiniMapY):
        self.MapX = MapX
        self.MapY = MapY
        self.MapSegmentsX = MiniMapX
        self.MapSegmentsY = MiniMapY

