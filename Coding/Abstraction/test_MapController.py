# -*- coding: utf-8 -*-
"""
Created on Fri Dec  8 13:06:05 2017

@author: JHMLap
"""

import pytest
import numpy as np
from MapController import *
from UnitController import *
from Unit import *

def test_MapController_initWorld():
    a = MapController(100,100,10,10)
    heightmap = np.random.rand(10,10)
    a._initWorld(heightmap)
    assert(np.sum(np.sum(a.terrainMap, axis=0)[[10*i for i in range(10)]]/10 - np.sum(heightmap, axis = 0)) < 0.0001)
    assert(np.sum(np.sum(a.terrainMap, axis=1)[[10*i for i in range(10)]]/10 - np.sum(heightmap, axis = 1)) < 0.0001)
    
def test_MapSegmentSets():
    a = MapController(100,100,10,10)
    heightmap = np.random.rand(10,10)
    a._initWorld(heightmap)
    a._initializeMapSegmentSets(10,10)
    re = a.returnSegments(0,0, Xrad = 49, Yrad = 49)
    assert(len(re[0]) == 5)   ## segments are 10 by 10, with 0-5 in x and 0 to 5 in y we should get 5x and 5y segments
    assert(len(re[1]) == 5)
    re = a.returnSegments(25,55, Xrad = 2, Yrad = 2)
    assert(len(re[0]) == 1)
    re = a.returnSegments(50,50, Xrad = 50, Yrad = 50)
    assert(len(re[0]) == 10)
    
def test_findmatches():
    a = MapController(100,100,10,10)
    heightmap = np.random.rand(10,10)
    a._initWorld(heightmap)
    a._initializeMapSegmentSets(10,10)
    Unit.mapController = a
    Unit.unitController = UnitController()
    Unit.unitController.registerMapController(a)
    u1 = Unit(15, 15, 44, 1, {'type':44,'player_id':1,'selected':0})
    seg = a.returnSegments(15,15)
    seg[0][0].add(u1)
    seg[1][0].add(u1)
    u2 = Unit(25, 25, 45, 1, {'type':45,'player_id':1,'selected':0})
    seg = a.returnSegments(25,25)
    seg[0][0].add(u2)
    seg[1][0].add(u2)
    u3 = Unit(10, 5, 44, 1, {'type':44,'player_id':1,'selected':0})
    seg = a.returnSegments(10,5)
    seg[0][0].add(u3)
    seg[1][0].add(u3)
    
    retun = a.returnSegmentUnits(15,15,50,50)
    assert(len(retun)==3)
    
    retun = a.returnSegmentUnits(15,15,2,2)
    assert(len(retun)==1)
    
    retun = a.returnSegmentUnits(15,15,10,10)
    assert(len(retun)==3)
    
    u = UnitController()
    u.registerMapController(a)
    
    matches = u.findMatches(20, 22,matches = 1, attributes = None, xrange = 6, yrange = 4, accountedUnits = False)
    assert(len(matches) == 1)
    
    matches = u.findMatches(20, 22,matches = 1, attributes = None, xrange = 20, yrange = 20, accountedUnits = False)
    assert(len(matches) == 1)
    
    matches = u.findMatches(20, 22,matches = 3, attributes = None, xrange = 20, yrange = 20, accountedUnits = False)
    assert (len(matches) == 3)
    
    matches = u.findMatches(20, 22,matches = 2, attributes = {'type':45}, xrange = 20, yrange = 20, accountedUnits = False)
    assert(len(matches) == 1)
    
    matches = u.findMatches(20, 22,matches = 3, attributes = {'type':44}, xrange = 20, yrange = 20, accountedUnits = False)
    assert(len(matches) == 2)
    
    matches = u.findMatches(20, 22,matches = 3, attributes = {'type':44}, xrange = 10, yrange = 10, accountedUnits = False)
    assert(len(matches) == 1)

def test_updateLookedAt():
      a = MapController(100,100,10,10)
      heightmap = np.random.rand(10,10)
      HeightMap = np.ones((15,15))
      a._initWorld(heightmap)
      a._initializeMapSegmentSets(10,10)
      a.updateLookedAt(90, 90, 100, 100, HeightMap)
      assert(np.sum(a.terrainMap[90:100,90:100]) == 0)
     
    
test_MapController_initWorld()
test_MapSegmentSets()
test_findmatches()
test_updateLookedAt()