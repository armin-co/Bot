from pysc2.agents.Abstraction.Unit import *
from skimage.measure import label
import numpy as np

class UnitController(object):
    def registerObsParser(self, obsParser):
        self.obsParser = obsParser
        
    def registerMapController(self, mapController):
        self.mapController = mapController
        Unit.registerController(mapController, self)
    
    def updateUnits(self):
        self.selectedUnits = []
        self.trackedUnits = self.newTrackedUnits
        self.newTrackedUnits = []
        self.round += 1
        self.unaccountedUnits = []
        self.newSightOrWayOff = []
        self.maybeNewSightOrWayOff = []
        self.unaccountedBlobs = []
        self.mapController._initializeMapSegmentSets(self.obsParser.camera.shape[0],self.obsParser.camera.shape[1])
        for unit in self.trackedUnits:
            unit.update()
            self.unaccountedUnits.append(unit)
            if unit.attributes['selected'] == 1:
                self.selectedUnits.append(unit)
        
    def accountFor(self,unit):
      unit.accounted = True
      self.unaccountedUnits.remove(unit)
		
    def findMatches(self,wX, wY,matches = 1, attributes = None, xrange = 10, yrange = 10, accountedUnits = False):
        possibleUnits = self.mapController.returnSegmentUnits(wX,wY,xrange,yrange)
        attributeMatch = []
        distance = []
        for unit in possibleUnits:
            if unit.accounted == True and accountedUnits == False:
                continue
            if abs(unit.xPos-wX) < xrange and abs(unit.yPos-wY) < yrange:
                if attributes:
                    allmatch = True
                    for key in attributes.keys():
                        if unit.attributes[key] == attributes[key]:
                            continue
                        else:
                            allmatch = False
                    if allmatch:
                        attributeMatch.append(unit)
                        distance.append( (unit.xPos-wX)**2 + (unit.yPos-wY)**2 )
                else:
                    attributeMatch.append(unit)
                    distance.append( (unit.xPos-wX)**2 + (unit.yPos-wY)**2 )
        returnlist = []
        if matches == 0:
            returnlist = attributeMatch
        else:
            counter = 0
            while (counter < matches):
                if counter >= len(attributeMatch):
                    break
                returnlist.append(attributeMatch[np.argmax(distance)])
                distance[np.argmax(distance)] = 999999999
                counter += 1
        return returnlist
    
    def newSightOrWayOff(self, unit): #unit is definitely there but was not expected
            self.newSightOrWayOff.append(unit)
        

    def maybeNewSightOrWayOff(self,unit): #unit was not expected but may be seen (not 100% sure)
        self.maybeNewSightOrWayOff.append(unit)
   
    def unaccountedBlob(self, wX,wY, blob, attributes):
        self.unaccountedBlobs.append([[wX,wY],blob,attributes])
        
    def blobsAdd(self, bloblist):
        self.blobs = bloblist
        
    def resolveMismatches(self):
       # we now have a list of unmatched units and unmatched blobs - all units we can easily track are gone
       #we still should first check if our blob is a single instance or if it is part of a bigger blob
       
       # in 2 consecutive observations we cant be off by too much in the minimapblobs, so we will move nearby unaccounted units to them first
       labelmap = label(((self.obsParser.Mplayer_relative*(self.obsParser.camera == 0)) > 0))  #all blobs off camera
       newAccountedBlobs = []
       for blobList in self.unaccountedBlobs:
           blob = blobList[1]
           wX = blobList[0][0]
           wY = blobList[0][1]
           blobbattributes = blobList[2]
           investigateBlob = True
           if len(np.transpose((labelmap == labelmap[blob[0],blob[1]]).nonzero())) > 1:
              #blob is part of a bigger group, so we have probably accounted units, that are in this blobs neighbour. we cannot know for sure and should not bother
              #check if at least one of the blobgroup is accounted 
              oneInList = False
              for nblob in np.transpose((labelmap == labelmap[blob[0],blob[1]]).nonzero()):
                 if (nblob[0],nblob[1]) in self.unaccountedBlobs:
                         continue
                 else:
                         oneInList = True
              if oneInList:
                     investigateBlob = False
              else:
                     investigateBlob = True
           if investigateBlob:
                   #blob is a new blob and may be worthy of investigation
                   matches = self.findMatches(wX,wY,matches = 0, attributes = None, xrange = 1.5* self.obsParser.WorldXCells/self.obsParser.camera.shape[0],yrange = 1.5* self.obsParser.WorldYCells/self.obsParser.camera.shape[1])
                   oneMatches = False 
                   for match in matches:
                       allMatch = True
                       for key in blobbattributes.keys():
                           try:
                                if blobbattributes[key] != match.attributes[key]:
                                   allMatch = False
                           except:
                                print("Abstraction: Key "+ str(key)+ " not found in:")
                                print(match.attributes.keys())
                       if allMatch:
                           match.accounted = True
                           newAccountedBlobs.append(blob)
                   if len(matches) == 0 or not oneMatches:
                        try:
                            if str(blobbattributes['player_relative']) == '3':
                                print("UnitController: new enemy spottet at "+str(wX)+", "+str(wY)+"!")
                            elif self.verbose:
                                print("UnitController: new Blob at "+str(wX)+", "+str(wY)+"!(neutral or friendly) Cant match it")
                        except:
                            if self.verbose:
                                print("UnitController: new generic Blob at "+str(wX)+", "+str(wY)+"! Cant match it")
					
					
					
       #now that the blobs are accounted for, lets match all remaining units we have not seen yet or have newly seen
	   	   
       for unit in self.newSightOrWayOff: #those are definitely there #these units are just coordinate dictionaries of seen points, not our tracked units
          if self.round < 2: #first round - we will see our startpoint for the first time
              newUnit = Unit(unit['x'], unit['y'], unit['type'], unit['player'], unit) # we create a trackable unit from the coordinate points
              self.trackedUnits.append(newUnit)
              newUnit.accounted = True
              continue
          #check if i have seen a double image (maybe a flying object splitting a big building etc)
          if len(self.findMatches(unit['x'], unit['y'],matches = 1, attributes = {'type':unit['type'], 'player':unit['player']}, xrange = 10, yrange = 10, accountedUnits = True)) >= 1:
             continue
          # we first check what faction this unit is. if it is ours and we have not created any new, then this unit is way off and we will grab the next unaccounted one from our unitinventory
          # if we created new units, well, than we should have tracked them. grab them from the unitdirectory
          # enemy and allys and neutrals can be created. does not matter
          wayOffMatches = self.findMatches(unit['x'],unit['y'],matches = 1, attributes = {'type':unit['type'], 'player':unit['player']}, xrange = self.obsParser.ScreenXCells*8, yrange = self.obsParser.ScreenYCells*8) 
          if len(wayOffMatches) == 1:
              wayOffMatches[0].WOCorrection(unit['x'],unit['y'])
              continue
          if unit['player'] != self.obsParser.player_id:  #neutral or enemy
              newUnit = Unit(unit['x'], unit['y'], unit['type'], unit['player'], unit)
              self.trackedUnits.append(newUnit)
              newUnit.accounted = True
              continue
          if self.verbose:
            print("UnitController: I think i saw a ghost at "+str(unit['x'])+" "+str(unit['y'])) #something funky is going on here
          newUnit = Unit(unit['x'], unit['y'], unit['type'], unit['player'], unit)
          self.trackedUnits.append(newUnit)
          newUnit.accounted = True
       for unit in self.maybeNewSightOrWayOff:
          # create neutrals and enemys for now. (neutrals only in the firs look (see mapcontroller))
          if self.round < 2:
              newUnit = Unit(unit['x'], unit['y'], unit['type'], unit['player'], unit)
              self.trackedUnits.append(newUnit)
              newUnit.accounted = True
              continue
          #remove double seen images
          if self.findMatches(unit['x'], unit['y'],matches = 1, attributes = {'type':unit['type'], 'player':unit['player'], 'hp':unit['hp'],"selected" : unit["selected"]}, xrange = 5, yrange = 5, accountedUnits = True):
             continue
          if unit['player'] != self.obsParser.player_id: #neutral and enemy units will be created even when we are not sure (better to overestimate an enemy, and neutral units do not move anyways)
              newUnit = Unit(unit['x'], unit['y'], unit['type'], unit['player'], unit)
              self.trackedUnits.append(newUnit)
              newUnit.accounted = True
              continue
          else: # hallucination by messy tracking
			           pass
	  
      
	    #now lets look at our tracked units which have not been matched (and are probably dead)
       for unit in self.unaccountedUnits:
          #if it is our own unit
          #check if we lost a unit lately and if it has a flag for enemy contact
          #if yes kill it, if no create distance matrix to blobs nearby (except screen) and move it to the nearest blob
          #if unit.lastCorrection > self.round-10:
              #self.trackedUnits.remove(unit)
          if unit.attributes['player'] != self.obsParser.player_id:
              unit.strike() # 1 strike and you are out!
              self.trackedUnits.remove(unit)
          #for allys:
          #kill unit (better to underestimate them for now)

    def __init__(self):
        self.round = 0
        self.trackedUnits = []
        self.newTrackedUnits = []
        self.verbose = False