
'''
+------------------------------------------------------------------------------+
|                                                                              |
| Author:       Martin Sura                                                    |
|                                                                              |
| Description:  This module is used to handle the process of creating,         |
|               upgrading and monitoring buildings in StarCraft II.            |
|               The process is initiated by a request of the agent / the       |
|               build order and consists of the following steps:               |
|                                                                              |
|               - Determine build positions                                    |
|               - Validate build positions using height map and existing       |
|                 buildings as reference                                       |
|               - Track buildings and update their state to identify finished  |
|                 buildings                                                    |
|               - Upgrade existing buildings by request                        |
|                 (e.g. TechLab on a Barack)                                   |
|                                                                              |
+------------------------------------------------------------------------------+
'''

from pysc2.lib import actions, features
import pysc2.agents.UnitDictionary.unitDictionary as ud

import numpy as np
np.set_printoptions(threshold=np.inf)

# Feature Indices
IDX_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index
IDX_UNIT_HIT_POINTS = features.SCREEN_FEATURES.unit_hit_points.index
IDX_HEIGHT_MAP = features.SCREEN_FEATURES.height_map.index

# Fixed building IDs
ID_BASE = { 'Terr' : 18, 'Zerg' : 86, 'Prot' : 59 }
ID_VESPENE_MINE = { 'Terr' : 20, 'Zerg' : 88, 'Prot' : 61 }
ID_EXTERNAL_UPGRADE_BUILDING = [ 21, 27, 28 ]   # Terran Barrack, Factory, Starport

# Fixed unit IDs
ID_VESPENE_GEYSER = 342
ID_MINERAL_FIELDS = [ 146, 147, 341, 483 ]

# Fixed action IDs
ACTION_ID_MOVE_CAMERA = actions.FUNCTIONS.move_camera.id
ACTION_ID_SELECT_POINT = actions.FUNCTIONS.select_point.id

# Fixed Parameters
SCREEN = [0]
MINIMAP = [1]
UNDEFINED = -1
UNUSED = 0
USED = 1

EXTERNAL_UPGRADE_OFFSET = 3
FORBIDDEN_AREA_OFFSET = 0#5
MINERAL_SEARCH_RADIUS = 40
BUILDING_SEPARATION = 1
BASE_SIZE_SCREEN = 19.0
BASE_RADIUS = 2.5

AVAILABILITY_CHECK_INTERVAL = 3

# Class to provide functionality of the building processes.
class ConstructionBuilder():

    # Possible states of the construction builder
    STATE_READY = 0
    STATE_UPDATING = 1
    STATE_BUILDING = 2
    STATE_DONE = 3

    # Function to initialize the ConstructionBuilder.
    def __init__(self, actionQueue=None, abstractionHub=None, race='Terr', debugOutputEnabled=False):
        self.actionQueue = actionQueue                          # Queue from agent to add actions
        self.abstractionHub = abstractionHub                    # AbstractionHub instance
        self.debugOutputEnabled = debugOutputEnabled            # Flag to enable or disable debug output
        self.race = race                                        # Current race the agent uses
        self.buildings = {}                                     # Dictionary to store and handle buildings. {sc2id : [Building, Building, ...]}
        self.vespeneLocations = {}                              # Store vespene geyser locations. {baseIds: [[(minimapX, minimapY), (screenX, screenY), isUsed_Flag], ..]}
        self.updateBuildingIdx = UNDEFINED                      # Index to use to identify next building to update
        self.updateBuildingHp = False                           # Flag to control updating und selecting buildings
        self.updateBuildingList = []                            # List of buildings to update
        self.tmpNewBuilding = None                              # Temporary instance of new building to build
        self.buildingToUpgrade = None                           # Instance of building to upgrade
        self.validationScreens = []                             # List of validation screens which contain usable and unusable positions
        self.nextValidation = (ID_BASE[self.race], 0)           # Information for next validation. (buildingId, baseIdx)
        self.availabilityCheckCounter = 0                       # Counter for checking buildings for availability in a specific interval
        
        # Current state of the construction builder
        self.currentState = ConstructionBuilder.STATE_READY

        # Create building instance to store main base
        baseBuildingId = ID_BASE[self.race]
        base = Building(buildingId=baseBuildingId, state=Building.STATE_FINISHED)
        self.addBuildingToDict(base)
        
        
    # Function to add new building request.
    def addNewBuilding(self, obs, buildingId, baseIdx=0):
        actionId = ud.getBuildActionIdById(buildingId)
        
        if actionId in obs.observation["available_actions"]:
            if self.debugOutputEnabled:
                print("CONSTRUCTION BUILDER: Add new building")
                
            (x,y) = (UNDEFINED, UNDEFINED)
            
            # Build base next to new ressource fields
            if buildingId == ID_BASE[self.race]:
                if self.debugOutputEnabled:
                    print("CONSTRUCTION BUILDER: Trying to build next base")
                    # TODO: Add implementation to build next base
            
            # Move screen to building reference point and create temporary building instance
            else:
                minimapRef = self.buildings[ID_BASE[self.race]][baseIdx].minimapRefPosition
                self.tmpNewBuilding = Building(buildingId, Building.STATE_REQUESTED, (0,0), (0,0), minimapRef, baseIdx)
                
                self.actionQueue.put( actions.FunctionCall(ACTION_ID_MOVE_CAMERA, [ [minimapRef[0], minimapRef[1]] ]) )
                self.currentState = ConstructionBuilder.STATE_BUILDING

        # TODO: Handle case if action is not available
        else:
            if self.debugOutputEnabled:
                print("CONSTRUCTION BUILDER: Action not available. ", actionId)
        
        
    # Function to determine the final build position and start the building process
    def startBuildProcess(self, obs):
        
        self.updateValidationScreen(obs)
        
        # Update base screen position and vespene geyser positions once
        if len(list(self.buildings.values())[0]) == 1:
            unitType = obs.observation["screen"][IDX_UNIT_TYPE]
            unitY, unitX = (unitType == ID_BASE[self.race]).nonzero()
            self.buildings[ID_BASE[self.race]][0].screenPosition = ( int(unitX.mean()), int(unitY.mean()) )
        
        (screenX, screenY) = (UNDEFINED, UNDEFINED)
        
        # Use Vespene geyser locations to build refinery, assimilator or extractor depending on race
        if self.tmpNewBuilding.buildingId == ID_VESPENE_MINE[self.race]:
            
            # Store vespene geyser positions depending on baseIdx if it does not already exist
            baseIdx = self.tmpNewBuilding.baseIdx
            self.storeVespenGeyserPositions(obs, baseIdx)
            
            # Get unused vespene geyser screen location
            for i in range(0, len(self.vespeneLocations[baseIdx])):
                if self.vespeneLocations[baseIdx][i][2] == UNUSED:
                    (screenX, screenY) = self.vespeneLocations[baseIdx][i][1]
                    self.vespeneLocations[baseIdx][i][2] = USED
                    break
        
        # Determine build position for every other building
        else:
            (screenX, screenY) = self.determineBuildPosition(obs)
        
        # If necessary search free position by validation map
        if (screenX, screenY) == (UNDEFINED, UNDEFINED):
            (screenX, screenY) = self.findBuildPositionByValidationMap()
        
        # Start build process if positions are determined
        if (screenX, screenY) != (UNDEFINED, UNDEFINED):
            self.nextValidation = (self.tmpNewBuilding.buildingId, self.tmpNewBuilding.baseIdx)
            self.tmpNewBuilding.worldPosition = self.abstractionHub.S2WPos(screenX, screenY)
            self.tmpNewBuilding.screenPosition = (screenX, screenY)
            self.addBuildingToDict(self.tmpNewBuilding)
            if not self.tmpNewBuilding in self.updateBuildingList:
                self.updateBuildingList.append(self.tmpNewBuilding)
            self.tmpNewBuilding = None
            self.processBuildingRequest(obs)
        
        # Handle case if position is undefined
        else:
            if self.debugOutputEnabled:
                print("CONSTRUCTION BUILDER: Unable to find build position.")
        
        
    # Function to start the build process
    def processBuildingRequest(self, obs):
        if self.debugOutputEnabled:
            print("CONSTRUCTION BUILDER: Process building request")
        
        # Iterate through all buildings and start build process if their state is STATE_REQUESTED
        for key in self.buildings.keys():
            for building in self.buildings[key]:
            
                if self.debugOutputEnabled:
                    building.printAttribs()
                
                if building.state == Building.STATE_REQUESTED:
                    if building.actionId in obs.observation["available_actions"]:
                        self.actionQueue.put( actions.FunctionCall(building.actionId, [ SCREEN, [building.getScreenX(), building.getScreenY()] ]) )
                        building.state = Building.STATE_BUILDING
        
        self.currentState = ConstructionBuilder.STATE_UPDATING
        
        
    # Function to upgrade a given building by given upgrade
    def upgradeBuilding(self, obs, upgradeId, buildingId, baseIdx=0):
        if self.debugOutputEnabled:
            print("UPGRADE: %d with %d" % (buildingId, upgradeId))
        
        # Get building and move camera to reference position
        if self.buildingToUpgrade == None:
            building = None
            for key in self.buildings.keys():
                for b in self.buildings[key]:
                    # TODO: Handle case if multiple buildings with same id exist
                    if b.buildingId == buildingId:
                        building = b
                        break
                
                if building != None:
                    break
            
            self.actionQueue.put( actions.FunctionCall(ACTION_ID_MOVE_CAMERA, [ [building.getMinimapRefX(), building.getMinimapRefY()] ]) )
            self.buildingToUpgrade = building
            self.currentState = ConstructionBuilder.STATE_READY
            
        # Upgrade building
        else:
            if self.buildingToUpgrade != None:
                # Add actions to select and upgrade building
                if self.buildingToUpgrade.state == Building.STATE_FINISHED:
                    upgradeActionId = ud.getUpgradeActionIdbyId(self.buildingToUpgrade.buildingId, upgradeId)
                    
                    self.actionQueue.put( actions.FunctionCall(ACTION_ID_SELECT_POINT, [ SCREEN, [self.buildingToUpgrade.getScreenX(), self.buildingToUpgrade.getScreenY()] ]) )
                    self.actionQueue.put(actions.FunctionCall(upgradeActionId, [ SCREEN, [self.buildingToUpgrade.getScreenX(), self.buildingToUpgrade.getScreenY()] ]) )
                    
                    self.buildingToUpgrade = None
                    self.currentState = ConstructionBuilder.STATE_DONE

                # Check whether building is finished
                else:
                    self.trackBuildingHp(obs, self.buildingToUpgrade)
                    self.currentState = ConstructionBuilder.STATE_READY
            
            
    # Function to update all existing buildings
    def updateBuildings(self, obs):
        # Update next building
        if self.updateBuildingHp:
            # Track building HP and set STATE_FINISHED if build process finished
            if self.updateBuildingIdx < len(self.updateBuildingList):
                self.trackBuildingHp(obs, self.updateBuildingList[self.updateBuildingIdx])
                
            # Reset update flags if buildings are not finished yet
            if self.updateBuildingIdx >= (len(self.updateBuildingList) - 1):
                for building in self.updateBuildingList:
                    if building.state != Building.STATE_FINISHED:
                        building.updated = False
                    
                self.currentState = ConstructionBuilder.STATE_DONE
            
            self.updateBuildingIdx = UNDEFINED
            self.updateBuildingHp = False
            
        # Select next building position and add move screen action to queue
        else:
            for i in range(0, len(self.updateBuildingList)):
                if self.updateBuildingList[i].state == Building.STATE_BUILDING:
                    if not self.updateBuildingList[i].updated:
                        self.actionQueue.put( actions.FunctionCall(ACTION_ID_MOVE_CAMERA, \
                            [ [self.updateBuildingList[i].getMinimapRefX(), self.updateBuildingList[i].getMinimapRefY()] ]) )
                        self.updateBuildingIdx = i
                        break
            
            if self.updateBuildingIdx != UNDEFINED:
                self.updateBuildingHp = True
            else:
                self.currentState = ConstructionBuilder.STATE_DONE
        
        
    # Function to track the hit points of a building.
    def trackBuildingHp(self, obs, building):
        if self.debugOutputEnabled:
            print("CONSTRUCTION BUILDER: Check buildings HP")
            print(" ")
        
        unitType = obs.observation["screen"][IDX_UNIT_TYPE]
        unitY, unitX = (unitType == building.buildingId).nonzero()
        unitHp = obs.observation["screen"][IDX_UNIT_HIT_POINTS]
        
        xPos = building.getScreenX()
        yPos = building.getScreenY()
        
        if xPos in unitX and yPos in unitY:
            xIdx = np.where(xPos == unitX)#[0].astype(int)[0]
            yIdx = np.where(yPos == unitY)#[0].astype(int)[0]
            indices = np.intersect1d(xIdx, yIdx)
            if len(indices) > 0:
                idx = indices[0]
                currentHp = unitHp[unitY[idx]][unitX[idx]]
                
                if self.debugOutputEnabled:
                    print("HP: %d" % currentHp)

                if currentHp == building.maxHp:
                    building.state = Building.STATE_FINISHED
                    self.updateBuildingList.remove(building)
        
        else:
            if self.availabilityCheckCounter >= AVAILABILITY_CHECK_INTERVAL:
                building.state = Building.STATE_REQUESTED
                self.tmpNewBuilding = building
                self.currentState = ConstructionBuilder.STATE_BUILDING
                self.availabilityCheckCounter = 0
            else:
                self.availabilityCheckCounter = self.availabilityCheckCounter + 1
            
        building.updated = True
        
        if self.debugOutputEnabled:
            print(" ")
        
        
    # Function to determine a position to use for new building. Returns (screenX, screenY) coordinates.
    def determineBuildPosition(self, obs):
        if self.debugOutputEnabled:
            print("CONSTRUCTION BUILDER: Determine build position")
        
        (screenX, screenY) = (UNDEFINED, UNDEFINED)
        buildingScreenSize = self.calcBuildingSizeScreen(self.tmpNewBuilding.buildingId)
        if self.debugOutputEnabled:
            print('BS: ', buildingScreenSize)
        base = self.buildings[ID_BASE[self.race]][self.tmpNewBuilding.baseIdx]
        searchPosition = True
        
        # Determine forbidden area (0: top-left, 1: bottom-right)
        (forbiddenX0, forbiddenX1, forbiddenY0, forbiddenY1) = self.determineForbiddenArea(obs)
        
        # Search best build position
        for key in self.buildings.keys():
            for building in self.buildings[key]:
                
                if self.tmpNewBuilding.minimapRefPosition == building.minimapRefPosition:
                    # Choose existing building as reference for new build position
                    refX = building.getScreenX()
                    refY = building.getScreenY()
                    refScreenSize = self.calcBuildingSizeScreen(building.buildingId)
                    if self.debugOutputEnabled:
                        print('RS: ', refScreenSize)
                    
                    buildDistance = int( round( (buildingScreenSize + refScreenSize) / 2.0 ) + BUILDING_SEPARATION )
                    additionalOffset = 0
                    
                    if self.tmpNewBuilding.buildingId in ID_EXTERNAL_UPGRADE_BUILDING:
                        additionalOffset = EXTERNAL_UPGRADE_OFFSET
                    
                    # Get new position candidates
                    newPositions = []
                    newPositions.append( (refX + buildDistance, refY) )
                    newPositions.append( (refX, refY - buildDistance) )
                    newPositions.append( (refX, refY + buildDistance) )
                    newPositions.append( (refX - buildDistance - additionalOffset, refY) )  # External upgrades are build on right side of the building
                    
                    # Choose valid position candidates depending on forbidden area and distance to other buildings
                    validPositions = []
                    for i in range(0, len(newPositions)):
                        x = newPositions[i][0]
                        y = newPositions[i][1]

                        if ( x < forbiddenX0 or x > forbiddenX1 or y < forbiddenY0 or y > forbiddenY1 ):
                            allDistancesValid = True
                            
                            # Check distances to all other buildings
                            for k in self.buildings.keys():
                                for b in self.buildings[k]:
                                    if self.tmpNewBuilding.minimapRefPosition == b.minimapRefPosition:
                                        bScreenSize = self.calcBuildingSizeScreen(b.buildingId)
                                        distance = self.calcDistance(x, y, b.getScreenX(), b.getScreenY())
                                        additionalOffset = 0
                                        
                                        if (b.buildingId in ID_EXTERNAL_UPGRADE_BUILDING and b.getScreenX() < x) \
                                        or (self.tmpNewBuilding.buildingId in ID_EXTERNAL_UPGRADE_BUILDING and x < b.getScreenX()):
                                            additionalOffset = EXTERNAL_UPGRADE_OFFSET
                                            
                                        minDistance = int( round( (buildingScreenSize + bScreenSize) / 2.0 ) + BUILDING_SEPARATION )
                                        if ( b.screenPosition == (x,y) or distance < (minDistance + additionalOffset) ):
                                            allDistancesValid = False
                                            break
                                        
                                if not allDistancesValid:
                                    break
                            
                            # Use height map to check for flat ground
                            if allDistancesValid:
                                heightMap = obs.observation["screen"][IDX_HEIGHT_MAP]
                                if x >= 0 and x < len(heightMap[0]) and y >= 0 and y < len(heightMap):
                                    halfSize = int( round( buildingScreenSize / 2.0 ) )
                                    xData = np.arange(x-halfSize, x+halfSize+1)
                                    yData = np.arange(y-halfSize, y+halfSize+1)
                                    height = heightMap[y][x]
                                    
                                    for i in range(0, len(xData)):
                                        for j in range(0, len(yData)):
                                            if xData[i] >= len(heightMap[0]) or yData[j] >= len(heightMap) or heightMap[yData[j]][xData[i]] != height:
                                                allDistancesValid = False
                                                if self.debugOutputEnabled:
                                                    print("Position inside uneven height area")
                                                break
                                        if not allDistancesValid:
                                            break
                                else:
                                    allDistancesValid = False
                            else:
                                if self.debugOutputEnabled:
                                    print("Position intersects other buildings.")
                            
                            # TODO: Race dependent ground check
                                        
                            if allDistancesValid:
                                validPositions.append( (x,y) )
                            
                        else:
                            if self.debugOutputEnabled:
                                print("Position inside forbidden area")
                    
                    if self.debugOutputEnabled:
                        print("---")
                        print("Forbidden Area: (%d, %d), (%d, %d)" % (forbiddenX0, forbiddenY0, forbiddenX1, forbiddenY1))
                        print("Possible Positions: %a" % newPositions)
                        print("Valid Positions: %a" % validPositions)
                        print("---")
                        
                    # If valid positions are available use the one which is closest to main base
                    if len(validPositions) > 0:
                        closestIdx = 0
                        closestDistance = np.inf

                        for i in range(0, len(validPositions)):
                            distance = self.calcDistance(base.getScreenX(), base.getScreenY(), validPositions[i][0], validPositions[i][1])
                            if distance < closestDistance:
                                closestDistance = distance
                                closestIdx = i

                        screenX = validPositions[closestIdx][0]
                        screenY = validPositions[closestIdx][1]
                        searchPosition = False
                        break
            
            if not searchPosition:
                break
            
        return (screenX, screenY)
        
        
    # Function to determine the forbidden area (area between base and minerals)
    def determineForbiddenArea(self, obs):
        base = self.buildings[ID_BASE[self.race]][self.tmpNewBuilding.baseIdx]
        mineralsX = []
        mineralsY = []
        
        forbiddenX0 = 0
        forbiddenY0 = 0
        forbiddenX1 = 0
        forbiddenY1 = 0
        
        # Get mineral field positions from screen
        for i in range(0, len(ID_MINERAL_FIELDS)):
            unitType = obs.observation["screen"][IDX_UNIT_TYPE]
            unitY, unitX = (unitType == ID_MINERAL_FIELDS[i]).nonzero()
            mineralsX.extend(unitX)
            mineralsY.extend(unitY)
            
        # Determine area between mineral fields and base
        if mineralsX != [] and mineralsY != []:
            baseX = base.getScreenX()
            baseY = base.getScreenY()
            
            mineralsMaxX = np.max(mineralsX)
            mineralsMinX = np.min(mineralsX)
            mineralsMaxY = np.max(mineralsY)
            mineralsMinY = np.min(mineralsY)
            
            # Top left corner
            if baseX <= mineralsMinX:
                forbiddenX0 = baseX
            else:
                forbiddenX0 = mineralsMinX
            
            if baseY <= mineralsMinY:
                forbiddenY0 = baseY
            else:
                forbiddenY0 = mineralsMinY
            
            # Bottom right corner
            if baseX >= mineralsMaxX:
                forbiddenX1 = baseX
            else:
                forbiddenX1 = mineralsMaxX
            
            if baseY >= mineralsMaxY:
                forbiddenY1 = baseY
            else:
                forbiddenY1 = mineralsMaxY
            
            # Increase forbidden area cause it uses center positions to create bounds
            forbiddenX0 = forbiddenX0 - FORBIDDEN_AREA_OFFSET
            forbiddenY0 = forbiddenY0 - FORBIDDEN_AREA_OFFSET
            forbiddenX1 = forbiddenX1 + FORBIDDEN_AREA_OFFSET
            forbiddenY1 = forbiddenY1 + FORBIDDEN_AREA_OFFSET
            
        return (forbiddenX0, forbiddenX1, forbiddenY0, forbiddenY1)
        
    
    # Function to use validation map to find free build position
    def findBuildPositionByValidationMap(self):
        if self.debugOutputEnabled:
            print("CONSTRUCTION BUILDER: Search free position by validation map")
        
        (screenX, screenY) = (UNDEFINED, UNDEFINED)
        buildingScreenSize = self.calcBuildingSizeScreen(self.tmpNewBuilding.buildingId)
        halfSize = int( round( buildingScreenSize / 2.0 ) )
        
        validationMap = np.array(self.validationScreens[self.tmpNewBuilding.baseIdx])
        freeX, freeY = (validationMap == 1).nonzero()
        
        for i in range(0, len(freeX)):
            dataValid = True
            x = freeX[i]
            y = freeY[i]
            
            xData = np.arange(x-halfSize-BUILDING_SEPARATION, x+halfSize+BUILDING_SEPARATION+1)
            yData = np.arange(y-halfSize-BUILDING_SEPARATION, y+halfSize+BUILDING_SEPARATION+1)
            
            for j in range(0, len(xData)):
                for k in range(0, len(yData)):
                    if xData[j] >= len(validationMap[0]) or yData[k] >= len(validationMap) or validationMap[yData[k]][xData[j]] != 1:
                        dataValid = False
                        break
                        
                if not dataValid:
                    break
                
            if dataValid:
                screenX = x
                screenY = y
                break
                
        return (screenX, screenY)
        
        
    # Function to update the validation screen which determines usable and unusable areas to build
    def updateValidationScreen(self, obs):
        
        buildingId = self.nextValidation[0]
        baseIdx = self.nextValidation[1]
        
        # Create initial validation map using height map
        if self.validationScreens == [] or baseIdx == len(self.validationScreens):
            heightMap = obs.observation["screen"][IDX_HEIGHT_MAP]
            base = self.buildings[ID_BASE[self.race]][baseIdx]
            validHeight = heightMap[base.getScreenY()][base.getScreenX()]
            validationMap = np.zeros([len(heightMap), len(heightMap[0])])
            
            # Create initial validation map using the height map
            for r in range(0, len(validationMap)):
                for c in range(0, len(validationMap[0])):
                    if heightMap[r][c] == validHeight:
                        validationMap[r][c] = 1
            
            # Add mineral field data points to validation map
            for i in range(0, len(ID_MINERAL_FIELDS)):
                unitType = obs.observation["screen"][IDX_UNIT_TYPE]
                unitY, unitX = (unitType == ID_MINERAL_FIELDS[i]).nonzero()
                if unitX != [] and unitY != []:
                    for j in range(0, len(unitX)):
                        validationMap[unitY[j]][unitX[j]] = 0
            
            self.validationScreens.append(validationMap)
            
        # Add building data points to validation map
        unitType = obs.observation["screen"][IDX_UNIT_TYPE]
        unitY, unitX = (unitType == buildingId).nonzero()
        
        if unitX != [] and unitY != []:
            for i in range(0, len(unitX)):
                self.validationScreens[baseIdx][unitY[i]][unitX[i]] = 0
        
        #print(self.validationScreens[baseIdx])
        

    # Function to get base position
    def storeBasePosition(self, obs):
        # TODO: Handle multiple bases
        baseIdx = 0
        units = self.abstractionHub.giveAllUnits()
        for i in range(0, len(units)):
            if units[i].type == ID_BASE[self.race]:
                self.buildings[units[i].type][baseIdx].worldPosition = ( int(units[i].xPos), int(units[i].yPos) )
                self.buildings[units[i].type][baseIdx].minimapRefPosition = self.abstractionHub.W2MPos(int(units[i].xPos), int(units[i].yPos))
                
                unitType = obs.observation["screen"][IDX_UNIT_TYPE]
                unitY, unitX = (unitType == ID_BASE[self.race]).nonzero()
                if unitY != [] and unitX != []:
                    self.buildings[units[i].type][baseIdx].screenPosition = ( int(unitX.mean()), int(unitY.mean()) )
                    
                break
        
        
    # Function to store vespene geyser positions
    def storeVespenGeyserPositions(self, obs, baseIdx):
        if self.vespeneLocations == {} or (not baseIdx in self.vespeneLocations.keys()):
            unitType = obs.observation["screen"][IDX_UNIT_TYPE]
            unitY, unitX = (unitType == ID_VESPENE_GEYSER).nonzero()
            
            # Exactly two separated vespene locations are visible on screen
            minimapRef = self.buildings[ID_BASE[self.race]][baseIdx].minimapRefPosition
            geyserPos1 = ( int( unitX[0:int(len(unitX) / 2.0)].mean() ), int( unitY[0:int(len(unitY) / 2.0)].mean() ) )
            geyserPos2 = ( int( unitX[int(len(unitX) / 2.0):len(unitX)].mean() ), int( unitY[int(len(unitY) / 2.0):len(unitY)].mean() ) )
            geyserData = [[ minimapRef, geyserPos1, UNUSED ], [ minimapRef, geyserPos2, UNUSED ]]
            self.vespeneLocations[baseIdx] = geyserData
            
        
    # Function to calculate the distance between two points
    def calcDistance(self, x0, y0, x1, y1):
        return ( np.sqrt( (x1 - x0)**2 + (y1 - y0)**2 ) )
        
        
    # Function to calculate building size on screen depending on radius
    def calcBuildingSizeScreen(self, buildingId):
        buildingRadius = float( ud.getUnitDataById(buildingId)['radius'] )
        buildingScreenSize = int( round( buildingRadius * (BASE_SIZE_SCREEN / BASE_RADIUS) ) )
        return (buildingScreenSize)
        
        
    # Function to return the current state of the construction builder
    def getCurrentState(self):
        return (self.currentState)
        
        
    # Function to get the list of existing buildings by id
    def getListOfBuildingsById(self, buildingId):
        selectedBuildings = []
        if buildingId in self.buildings:
            selectedBuildings = self.buildings[buildingId]
        return selectedBuildings 
        
        
    # Function to return all built vespene mines as list
    def getVespeneMines(self):
        return self.getListOfBuildingsById(ID_VESPENE_MINE[self.race])
        
        
    # Function to set the current state of the construction builder
    def setCurrentState(self, currentState):
        self.currentState = currentState
        
        
    # Function to add a building to dictionary of buildings
    def addBuildingToDict(self, building):
        key = building.buildingId
        try:
            if not building in self.buildings[key]:
                self.buildings[key].append(building)
        except:
            self.buildings[key] = [building]
        
        
    # Function to remove a building from dictionary
    def removeFromDict(self, building):
        self.buildings[building.buildingId].remove(building)
        
        
# Class to provide information of a Building
class Building():

    STATE_UNDEFINED = 0     # Undefined state.
    STATE_REQUESTED = 1     # Build process is requested but not started yet.
    STATE_BUILDING = 2      # Build process has started and has to be checked periodically.
    STATE_FINISHED = 3      # Build process finished. Building can be used.
    #STATE_DESTROYED = 4     # Building is destroyed. No longer available.

    # Function to initialize a Building
    def __init__(self, buildingId=0, state=STATE_UNDEFINED, worldPosition=(0,0), screenPosition=(42,42), minimapRefPosition=(0,0), baseIdx=0):
        self.buildingId = buildingId                                            # SC2 ID of the building
        self.actionId = ud.getBuildActionIdById(buildingId)                     # SC2 Action ID to start build process
        self.state = state                                                      # Building state
        self.worldPosition = worldPosition                                      # World Position depending on Abstraction
        self.screenPosition = screenPosition                                    # Screen Position depending on World Position
        self.minimapRefPosition = minimapRefPosition                            # Position on minimap as reference to move the camera
        self.baseIdx = baseIdx                                                  # Index to identify near which base this building is placed
        self.maxHp = int(ud.getUnitDataById(buildingId)['easy Hitpoints'])      # Miximum HP of the Building
        self.updated = False                                                    # Flag to determine whether building was updated in last update cycle
        self.isRallyPointSet = False                                            # Flag to determine whether rallypoint of the building is set
        
    # Specific Getters

    def getWorldX(self):
        return self.worldPosition[0]

    def getWorldY(self):
        return self.worldPosition[1]

    def getScreenX(self):
        return self.screenPosition[0]
        
    def getScreenY(self):
        return self.screenPosition[1]
    
    def getMinimapRefX(self):
        return self.minimapRefPosition[0]
    
    def getMinimapRefY(self):
        return self.minimapRefPosition[1]
        
    # Function to print attributes for debugging
    def printAttribs(self):
        print("BUILDING: BuildingID: %d, ActionID: %d, State: %d, X: %d, Y: %d, maxHP: %d" % \
        (self.buildingId, self.actionId, self.state, self.screenPosition[0], self.screenPosition[1], self.maxHp))
