
from pysc2.lib import actions, features

import numpy as np
np.set_printoptions(threshold=np.inf)


# Actions
ACTION_ID_NO_OP = actions.FUNCTIONS.no_op.id
ACTION_ID_SELECT_POINT = actions.FUNCTIONS.select_point.id

ACTION_ID_BUILD_REFINERY = actions.FUNCTIONS.Build_Refinery_screen.id
ACTION_ID_BUILD_SUPPLYDEPOT = actions.FUNCTIONS.Build_SupplyDepot_screen.id
ACTION_ID_BUILD_BARRACKS = actions.FUNCTIONS.Build_Barracks_screen.id
ACTION_ID_BUILD_TECHLAB = actions.FUNCTIONS.Build_TechLab_screen.id

# Features
IDX_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index
IDX_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index
IDX_UNIT_HIT_POINTS = features.SCREEN_FEATURES.unit_hit_points.index
IDX_MINERALS = 1
IDX_VESPENE = 2

# Unit IDs
UNIT_ID_TERRAN_SCV = 45
UNIT_ID_TERRAN_COMMANDCENTER = 18
UNIT_ID_TERRAN_REFINERY = 20
UNIT_ID_TERRAN_BARRACKS = 21
UNIT_ID_VESPENE_GEYSER = 342

# Parameters
PLAYER_SELF = 1
SCREEN = [0]
MAX_BARRACKS_COUNT = 2


# Class to provide functionality of the building process
class ConstructionBuilder():
    
    def __init__(self, actionQueue=None):
        # Queue from agent
        self.actionQueue = actionQueue
    
        # Flag to determine player position
        self.base_top_left = None
        
        # Flags to check build order
        self.scv_selected = False
        self.supply_depot_built = False
        self.refinery_built = False
        self.techlab_built = False
        self.do_wait_for_minerals = True
        self.barracks_count = 0
        
        # Storage for barrack locations
        self.vespene_locations = []
        self.barrack_locations = []
    
    
    # Function to determine position top_left or bottom_right of player on map Simple64
    def determinePosition(self, obs):
        if self.base_top_left is None:
            player_y, player_x = (obs.observation["minimap"][IDX_PLAYER_RELATIVE] == PLAYER_SELF).nonzero()
            self.base_top_left = player_y.mean() <= 31
    
    
    # Function to work with locations relative to player position
    def transformLocation(self, x, x_distance, y, y_distance):
        if not self.base_top_left:    
            return [x - x_distance, y - y_distance]
        else:
            return [x + x_distance, y + y_distance]
            
    
    # Function to build specific buildings (one supply, one barrack)
    def buildBuildings(self, obs):
        # Store vespene geyser locations
        if self.vespene_locations == []:
            print("STORE VESPENE GEYSER POSITIONS...")
            unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
            unit_y, unit_x = (unit_type == UNIT_ID_VESPENE_GEYSER).nonzero()
            self.vespene_locations.append( [ unit_x[0:int(len(unit_x) / 2.0)], unit_y[0:int(len(unit_y) / 2.0)] ] )
            self.vespene_locations.append( [ unit_x[int(len(unit_x) / 2.0):len(unit_x)], unit_y[int(len(unit_y) / 2.0):len(unit_y)] ] )
    
        # Wait for enough resources to build two barracks
        if (not self.do_wait_for_minerals) or obs.observation["player"][IDX_MINERALS] > 400:
            self.do_wait_for_minerals = False
            
            # Build supply depot
            if not self.supply_depot_built:
                print("SELECT SCV...")
                unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
                unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_SCV).nonzero()
                target = [unit_x[0], unit_y[0]]
                self.scv_selected = True
                self.actionQueue.put(actions.FunctionCall(ACTION_ID_SELECT_POINT, [SCREEN, target]))
                
                if ACTION_ID_BUILD_SUPPLYDEPOT in obs.observation["available_actions"]:
                    print("BUILD SUPPLY DEPOT...")
                    unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
                    unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_COMMANDCENTER).nonzero()
                    target = self.transformLocation(int(unit_x.mean()), 20, int(unit_y.mean()), 20)
                    self.supply_depot_built = True
                    self.actionQueue.put(actions.FunctionCall(ACTION_ID_BUILD_SUPPLYDEPOT, [SCREEN, target]))
            
            # Build two barracks
            elif self.barracks_count < MAX_BARRACKS_COUNT:
                print("SELECT SCV...")
                unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
                unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_SCV).nonzero()
                target = [unit_x[self.barracks_count+1], unit_y[self.barracks_count+1]]
                self.scv_selected = True
                self.actionQueue.put(actions.FunctionCall(ACTION_ID_SELECT_POINT, [SCREEN, target]))
                
                if ACTION_ID_BUILD_BARRACKS in obs.observation["available_actions"]:
                    print("BUILD BARRACK...")
                    unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
                    unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_COMMANDCENTER).nonzero()
                    target = [0,0]
                    
                    if self.barracks_count == 0:
                        target = self.transformLocation(int(unit_x.mean()), 0, int(unit_y.mean()), 20)
                    elif self.barracks_count == 1:
                        target = self.transformLocation(int(unit_x.mean()), 20, int(unit_y.mean()), 0)
                    
                    self.actionQueue.put(actions.FunctionCall(ACTION_ID_BUILD_BARRACKS, [SCREEN, target]))
                    self.barrack_locations.append(target)
                    self.barracks_count += 1
            
            # Build refinery
            elif not self.refinery_built:
                if ACTION_ID_BUILD_REFINERY in obs.observation["available_actions"]:
                    print("BUILD REFINERY...")
                    target = [int(self.vespene_locations[0][0].mean()), int(self.vespene_locations[0][1].mean())]
                    self.refinery_built = True
                    self.actionQueue.put(actions.FunctionCall(ACTION_ID_BUILD_REFINERY, [SCREEN, target]))
                    
            # Build Tech Lab
            elif not self.techlab_built and obs.observation["player"][IDX_VESPENE] > 30:
                print("SELECT BARRACK...")
                target = self.barrack_locations[0]
                self.actionQueue.put(actions.FunctionCall(ACTION_ID_SELECT_POINT, [SCREEN, target]))
                
                #print(obs.observation["available_actions"])
                if ACTION_ID_BUILD_TECHLAB in obs.observation["available_actions"]:
                    print("BUILD BARRACKS TECHLAB...")
                    self.techlab_built = True
                    self.actionQueue.put(actions.FunctionCall(ACTION_ID_BUILD_TECHLAB, [SCREEN, target]))
                    
    
    # Function to track the hp of a building
    def hpTracking(self):
        print(" ")
        
        unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
        unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_BARRACKS).nonzero()
        unit_hit_points = obs.observation["screen"][IDX_UNIT_HIT_POINTS]
        
        print("UNIT-X-LEN: %d" % len(unit_x))
        print("UNIT-Y-LEN: %d" % len(unit_y))
        print("HP-BOUNDS: %d x %d" % (len(unit_hit_points), len(unit_hit_points[0])))
        
        unit_xy_length = int((len(unit_x) + len(unit_y)) / 2)
        for i in range(0, unit_xy_length):
            print("HP: %d" % unit_hit_points[unit_y[i]][unit_x[i]])
        
        print(" ")
        