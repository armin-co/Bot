
# coding: utf-8

# In[1]:


import pandas as pd
import pickle
import os
import sys
import numpy as np


with open("units", "rb") as f:
    a = pickle.load(f)
with open("pickledUnitsEffectsAbilitiesWeaponsUpgrades", "rb") as f:
    [un2,ef2,ab2,wp2,ug2] = pickle.load(f)


# In[2]:


sc2number = {}
for nr in a.keys():
    try:
        sc2number[a[nr]] = nr
    except:
        print(a[nr])


# Combine the sc2 names with the units

# In[3]:


for key in un2.keys():
    try:
        un2[key]['sc2nr'] = sc2number[key]
    except:
        print(key)


# combine the weaponarray with the weapons

# In[25]:


for key in wp2.keys():
    try:
        wp2[key]['effect']
        
        ekey =  wp2[key]['effect']
        try:
                wp2[key]['effect'] = ef2[ekey]
        except:
                print()
                print(ekey)
                del wp2[key]['effect'][ekey]
                print(wp2[key]['effect'])
    except:
        pass


# In[26]:


for key in un2.keys():
    try:
        un2[key]['weaponarray']
        
        for wkey in un2[key]['weaponarray']:
            try:
                un2[key]['weaponarray'][wkey] = wp2[wkey]
            except:
                print()
                print(wkey)
                del un2[key]['weaponarray'][wkey]
                print(un2[key]['weaponarray'])
    except:
        pass
    
    
    


# In[30]:


effects = pd.DataFrame(ef2).T
effects.columns


# In[39]:


units = pd.DataFrame(un2).T
for i in units.columns:
    print(i)


# In[40]:


for key in un2.keys():
    try:
        un2[key]['effectarray']
        
        for wkey in un2[key]['effectarray']:
            try:
                un2[key]['effectarray'][wkey] = ef2[wkey]
            except:
                print()
                print(wkey)
                del un2[key]['effectarray'][wkey]
                print(un2[key]['effectarray'])
    except:
        pass
    
    
    


# In[41]:


for key in un2.keys():
    try:
        un2[key]['turretarray']
        
        for wkey in un2[key]['turretarray']:
            try:
                un2[key]['turretarray'][wkey] = wp2[wkey]
            except:
                print()
                print(wkey)
                del un2[key]['turretarray'][wkey]
                print(un2[key]['turretarray'])
    except:
        pass
    
    


# In[42]:


abilities = pd.DataFrame(ab2).T
for c in abilities.columns:
    print(c)


# In[43]:


for key in ab2.keys():
    try:
        ab2[key]['effectarray']
        
        for wkey in ab2[key]['effectarray']:
            try:
                ab2[key]['effectarray'][wkey] = ef2[wkey]
            except:
                print()
                print(wkey)
                del ab2[key]['effectarray'][wkey]
                print(ab2[key]['effectarray'])
    except:
        pass
    
    


# In[45]:


for key in ab2.keys():
    try:
        ab2[key]['effect']
        
        ekey =  ab2[key]['effect']
        try:
                ab2[key]['effect'] = ef2[ekey]
        except:
                
                try:
                    ab2[key]['effect'] = ef2[ekey[0]]
                except:
                    print()
                    print(ekey)
                    del ab2[key]['effect'][ekey]
                    print(ab2[key]['effect'])
    except:
        pass


# In[46]:


for key in ab2.keys():
    try:
        ab2[key]['calldowneffect']
        
        ekey =  ab2[key]['calldowneffect']
        try:
                ab2[key]['calldowneffect'] = ef2[ekey]
        except:
                
                try:
                    ab2[key]['calldowneffect'] = ef2[ekey[0]]
                except:
                    print()
                    print(ekey)
                    del ab2[key]['calldowneffect'][ekey]
                    print(ab2[key]['calldowneffect'])
    except:
        pass


# In[48]:


for key in un2.keys():
    try:
        un2[key]['abilarray']
        
        for wkey in un2[key]['abilarray']:
            try:
                un2[key]['abilarray'][wkey] = ab2[wkey]
            except:
                print()
                print(wkey)
                try:
                    wkey = int(wkey)
                    for i in un2[key]['abilarray'][wkey].keys():
                        try:
                            un2[key]['abilarray'][wkey] = ab2[i]
                        except:
                            pass
                except:
                    del un2[key]['abilarray'][wkey]
                    print(un2[key]['abilarray'])
    except:
        pass
    
    


# In[51]:


units = pd.DataFrame(un2).T


# In[52]:


units


# Adding simple Unit Statistics Scraped from other sites ( see scrapebook )

# In[55]:


unitarray = []
filelist = ['simpleUnitStat', 'simpleUnitStat2', 'simpleUnitStat3', 'simpleUnitStat4']
for file in filelist:
    with open(file,"rb") as f:
        unitarray.append(pickle.load(f))
        
alldic = {}
for uarr in unitarray:
    for key in uarr.keys():
        alldic[key] = uarr[key]
        
for unit in alldic.keys():
    newunit = {}
    for akey in alldic[unit].keys():
        if '-' in alldic[unit][akey] and len(alldic[unit][akey]) < 2:
            continue
        newunit["easy "+akey] = alldic[unit][akey]
    alldic[unit] = newunit


# In[56]:


alldic


# In[59]:


for key in alldic.keys():
    try:
        un2[key]
        for atr in alldic[key].keys():
            un2[key][atr] = alldic[key][atr]
    except:
        key2 = key.replace(" ","")
        try:
            un2[key2]
            for atr in alldic[key].keys():
                un2[key2][atr] = alldic[key][atr]
        except:
            print(key)
    


# In[58]:


print(un2.keys())


# In[63]:


alldickeys = ['Auto-Turret','Hellbat','Infested Terran','Locust','Mule','Nydus Worm','Siege Tank (siege)','Siege Tank (tank)',
'Swarm Host','Thor 2','Viking (am)','Viking (fm)','Warpgate','Templar Archives','Lurker Den']

un2keys = ['AutoTurret','Hellbat','InfestedTerransWeapon','LocustMP','MULE','NydusCanalCreeper','SiegeTankSieged','SiegeTank',
'SwarmHostMP','Thor 2','VikingAssault','VikingFighter','WarpGate','TemplarArchive','LurkerDenMP']

for i in range(len(un2keys)):
    try:
        for akey in alldic[alldickeys[i]].keys():
            un2[un2keys[i]][akey] = alldic[alldickeys[i]][akey]
    except:
        print(un2keys[i])
        dici = {}
        
        for akey in alldic[alldickeys[i]].keys():
            dici[akey] = alldic[alldickeys[i]][akey]
        un2[un2keys[i]] = dici


# In[64]:


units = pd.DataFrame(un2).T


# In[69]:


units.sc2nr


# In[73]:


keys = un2.keys()
tempdic = {}
for i in keys:
    try:
        nr = un2[i]['sc2nr']
        tempdic[nr] = un2[i]
    except:
        pass
for i in tempdic.keys():
    un2[i] = tempdic[i]


# In[74]:


with open("UnitDictionary(Pickled)", "wb") as f:
    pickle.dump(un2,f)

