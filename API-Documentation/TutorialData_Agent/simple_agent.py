# +-------------------------------------------------------------------------+
# |                                                                         |
# |  This Agent demonstrates some simple mechanics like building buildings  | 
# |  and units and using the army to attack the enemy.						|
# |                                                                         |
# |                                                                         |
# |  This file has to be placed in your pysc2/agents/ directory.			|
# |  To start this Agent use the following command:							|
# |                                                                         |
# |  python -m pysc2.bin.agent \											|
# |              --map Simple64 \											|
# |              --agent pysc2.agents.simple_agent.SimpleAgent \			|
# |              --agent_race T												|
# |                                                                         |
# |                                                                         |
# |  The implementation is based on this tutorial:							|
# |    https://chatbotslife.com/building-a-basic-pysc2-agent-b109cde1477c   |
# |                                                                         |
# +-------------------------------------------------------------------------+

from pysc2.agents import base_agent
from pysc2.lib import actions, features

import time


# Delay to slow down the game
DELAY_GAME = 0.00

# Actions
ACTION_ID_NO_OP = actions.FUNCTIONS.no_op.id
ACTION_ID_SELECT_POINT = actions.FUNCTIONS.select_point.id

ACTION_ID_BUILD_SUPPLYDEPOT = actions.FUNCTIONS.Build_SupplyDepot_screen.id
ACTION_ID_BUILD_BARRACKS = actions.FUNCTIONS.Build_Barracks_screen.id

ACTION_ID_TRAIN_MARINE = actions.FUNCTIONS.Train_Marine_quick.id
ACTION_ID_RALLY_UNITS_MINIMAP = actions.FUNCTIONS.Rally_Units_minimap.id

ACTION_ID_SELECT_ARMY = actions.FUNCTIONS.select_army.id
ACTION_ID_ATTACK_MINIMAP = actions.FUNCTIONS.Attack_minimap.id

# Features
IDX_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index
IDX_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index

# Unit IDs
UNIT_ID_TERRAN_SCV = 45
UNIT_ID_TERRAN_COMMANDCENTER = 18
UNIT_ID_TERRAN_BARRACKS = 21

# Parameters
PLAYER_SELF = 1
SCREEN = [0]
SUPPLY_USED = 3
SUPPLY_MAX = 4
MINIMAP = [1]
QUEUED = [1]
NOADD = [0]


# Class to provide an agent which builds units and attacks the enemy
class SimpleAgent(base_agent.BaseAgent):
	
	# Flag to determine player position
	base_top_left = None
	
	# Flags to check build order
	scv_selected = False
	supply_depot_built = False
	barracks_built = False
	barracks_selected = False
	barracks_rallied = False
	army_selected = False
	army_rallied = False
	
	
	# Setup function is executed once on agents startup
	def setup(self, obs_spec, action_spec):
		super(SimpleAgent, self).setup(obs_spec, action_spec)
		
	
	# Step function is executed each game step iteration.
	def step(self, obs):
		super(SimpleAgent, self).step(obs)
		actionToExecute = actions.FunctionCall(ACTION_ID_NO_OP, [])
		
		# Delay to slow down the game
		time.sleep(DELAY_GAME)
		
		# Determine position top_left or bottom_right of player on map Simple64
		if self.base_top_left is None:
			player_y, player_x = (obs.observation["minimap"][IDX_PLAYER_RELATIVE] == PLAYER_SELF).nonzero()
			self.base_top_left = player_y.mean() <= 31
		
		
		# +------------------------------------------------------------------------------+
		# |    Process some kind of build order (supply, barrack, marines, attacking)    |
		# +------------------------------------------------------------------------------+
		
		if not (self.supply_depot_built and self.barracks_built):
			# Do building stuff
			actionToExecute = self.buildBuildings(obs)
			
		elif self.barracks_built and (obs.observation["player"][SUPPLY_USED] < obs.observation["player"][SUPPLY_MAX]):
			# Use barracks to build marines until max unit number is reached
			actionToExecute = self.buildUnitInBarrack(obs, ACTION_ID_TRAIN_MARINE)
			
		else:
			# Use army to attack the enemy
			actionToExecute = self.attackEnemy(obs)
		
		# +------------------------------------------------------------------------------+
		
		return actionToExecute
	
	
	# Function to work with locations relative to player position
	def transformLocation(self, x, x_distance, y, y_distance):
		if not self.base_top_left:	
			return [x - x_distance, y - y_distance]
		else:
			return [x + x_distance, y + y_distance]
	

	# Function to attack the enemy with the whole army
	def attackEnemy(self, obs):
		action = actions.FunctionCall(ACTION_ID_NO_OP, [])
		
		if not self.army_rallied:
			if not self.army_selected:
				if ACTION_ID_SELECT_ARMY in obs.observation["available_actions"]:
					self.army_selected = True
					self.barracks_selected = False
					action = actions.FunctionCall(ACTION_ID_SELECT_ARMY, [[0]])
		
			elif ACTION_ID_ATTACK_MINIMAP in obs.observation["available_actions"]:
				self.army_rallied = True
				self.army_selected = False
				
				# Use attack point on hard coded position
				if self.base_top_left:
					action = actions.FunctionCall(ACTION_ID_ATTACK_MINIMAP, [[1], [39, 45]])
				else:
					action = actions.FunctionCall(ACTION_ID_ATTACK_MINIMAP, [[1], [21, 24]])
		
		return action

	
	# Function to build units in the barrack
	def buildUnitInBarrack(self, obs, trainUnitActionId):
		action = actions.FunctionCall(ACTION_ID_NO_OP, [])
		
		# Select barrack if neccessary
		if not self.barracks_selected:
			unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
			unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_BARRACKS).nonzero()
			
			if unit_y.any():	# use a barrack which is available
				target = [int(unit_x.mean()), int(unit_y.mean())]
				self.barracks_selected = True
				action = actions.FunctionCall(ACTION_ID_SELECT_POINT, [SCREEN, target])
				
		# set rally point before building units		
		elif not self.barracks_rallied:
			self.barracks_rallied = True
			
			# Set rally point on hard coded position (ramp near the base)
			if self.base_top_left:
				action = actions.FunctionCall(ACTION_ID_RALLY_UNITS_MINIMAP, [MINIMAP, [29, 21]])
			else:
				action = actions.FunctionCall(ACTION_ID_RALLY_UNITS_MINIMAP, [MINIMAP, [29, 46]])
		
		# Build marines
		elif obs.observation["player"][SUPPLY_USED] < obs.observation["player"][SUPPLY_MAX] \
				and trainUnitActionId in obs.observation["available_actions"]:
			action = actions.FunctionCall(trainUnitActionId, [QUEUED])
			
		return action
	
	
	# Function to build specific buildings (one supply, one barrack)
	def buildBuildings(self, obs):
		action = actions.FunctionCall(ACTION_ID_NO_OP, [])
	
		# Select scv if neccessary...
		if not self.scv_selected:
			unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
			unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_SCV).nonzero()
			target = [unit_x[0], unit_y[0]]
			self.scv_selected = True
			action = actions.FunctionCall(ACTION_ID_SELECT_POINT, [SCREEN, target])
		
		# ...and build buildings
		else:
			if not self.supply_depot_built:
				if ACTION_ID_BUILD_SUPPLYDEPOT in obs.observation["available_actions"]:
					unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
					unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_COMMANDCENTER).nonzero()
					target = self.transformLocation(int(unit_x.mean()), 0, int(unit_y.mean()), 20)
					self.supply_depot_built = True
					action = actions.FunctionCall(ACTION_ID_BUILD_SUPPLYDEPOT, [SCREEN, target])
					
			elif not self.barracks_built:
				if ACTION_ID_BUILD_BARRACKS in obs.observation["available_actions"]:
					unit_type = obs.observation["screen"][IDX_UNIT_TYPE]
					unit_y, unit_x = (unit_type == UNIT_ID_TERRAN_COMMANDCENTER).nonzero()
					target = self.transformLocation(int(unit_x.mean()), 20, int(unit_y.mean()), 0)
					self.barracks_built = True
					action = actions.FunctionCall(ACTION_ID_BUILD_BARRACKS, [SCREEN, target])
					
		return action


