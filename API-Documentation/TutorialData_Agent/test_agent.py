# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS-IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""A random agent for starcraft."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import socket
from pysc2.agents import base_agent
from pysc2.lib import actions
from pysc2.lib import features

class TestBot(base_agent.BaseAgent):
    """Unser TestBot"""
    def setup(self, obs_spec, action_spec):
        super(TestBot, self).setup(obs_spec, action_spec)
        self.socket = socket.socket()
        host = socket.gethostname()
        port = 999
        self.socket.bind((host,port))
        self.socket.listen(5)
        [self.connection, adress] = self.socket.accept()
        self.block = 0
    
    def step(self, obs):
        super(TestBot, self).step(obs)
        blockfurther = 1
        arguments = []
        while (blockfurther):
            blockfurther = 0
            var = self.connection.recv(1026)    
            if len(var.split()) > 0:
                var = var.split()
                self.block = int(var[0])
                actionID = int(var[1])
                pos = 2
                for i in range(int(var[2])):
                    pos = pos+1
                    innerargs = []
                    for z in range(int(var[pos])):
                        pos = pos+1
                        innerargs.append(int(var[pos]))
                    arguments.append(innerargs)
            else:
                blockfurther = self.block
        self.connection.send(str.encode("blocking: {0} executing actions.FunctionCall( {1} , {2} )".format(self.block,actionID,arguments)))
        return actions.FunctionCall(actionID, arguments)
        