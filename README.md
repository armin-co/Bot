# Starcraft2 Bot
In diesem Projekt befinden sich alle relevanten Dateien die zum Bot und
dem Projekt gehören.
Die vier Ordner enthalten folgende Inhalte:

## Api-Documentation
Eine Sammlung von Informationen, Links und Datein die mit der API(pysc2) und
Starcaft 2 zusammen hängen.
## Coding (Agent)
In dem Ordner Coding befindet sich der Pythoncode zum Bot aufgeteilt in
die einzelnen Module.
Wie man den Agenten/Code anwendet ist in der Dokumentation vollständig beschrieben.
## Documentation
Im Ordner Dokumentation befinden sich die um Projetk gehörende Dokumentation.
Die Datei document.pdf gibt dabei alle Informationen wieder.
## General-Guides
Allgemeine Informationen die in Verbindung mit diesem Projekt stehen.
